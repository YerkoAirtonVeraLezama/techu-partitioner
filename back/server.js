var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyparser = require('body-parser');

//const global = require('./global');
const controllers = require('./controllers/users');
const controllers_auth = require('./controllers/auth');
const controllers_global = require('./controllers/general');

//AGREGADO POR EL ENV
const URL_BASE = process.env.URL_BASE;

app.use(bodyparser.json());

app.listen(port, function () {
console.log('****** Corriendo servidor Node JS en prto '+ port + ' ******');
});

app.get(URL_BASE + 'users/:idUser',controllers.obtenerUsuarioXid);
app.post(URL_BASE + 'users',controllers.agregarUsuario);
app.put(URL_BASE + 'users',controllers.modificarUsuario);
app.delete(URL_BASE + 'users/:idUsuario',controllers.eliminarUsuario);
app.post(URL_BASE + 'login',controllers.login);
app.post(URL_BASE + 'logout',controllers.logout);

app.get(URL_BASE + 'users_mlab',controllers_global.obtenerUsuarios);
app.get(URL_BASE + 'users_mlab2',controllers_global.obtenerUsuariosId);

app.post(URL_BASE + 'login_mlab',controllers_auth.login);
app.post(URL_BASE + 'logout_mlab',controllers_auth.logout);