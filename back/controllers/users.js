var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyparser = require('body-parser');

var users_file = require('../users.json'); 
//const URL_BASE = '/tech-u-vera/v1/';

const global = require('../global');

app.use(bodyparser.json());

//Peticion GET simple
/*app.get(URL_BASE + 'users', function (req, response) {
	//response.send({"msg":"Operacion lanzada!!"});
	response.status(202).send(users_file);
});*/

//Peticion GET users con parametro id, el 1ro es un nombre cualquiera
//app.get(global.URL_BASE + 'users/:idUser',
function obtenerUsuarioXid(req,res){
	let posicion = req.params.idUser; //VARIABLE LOCAL DEL BLOQUE
	let size = users_file.length;

	console.log(size);

	let respuesta = (users_file[posicion] == undefined) ? {"msg":"Usuario no encontrado"} : users_file[posicion];

	//console.log("GET con idUsuario = " + req.params.idUser);
	res.send(respuesta);
}

//Peticion GET sin query
/*app.get(global.URL_BASE + 'users',function(req,res){
	res.send(users_file);
});/*

//Peticion GET con query
/*app.get(URL_BASE + 'users',function(req,res){
	console.log(req.query.id);
	console.log(req.query.nombre);
});*/

//Peticion POST
//app.post(global.URL_BASE + 'users',
function agregarUsuario(req,res){
	//console.log("Nuevo usuario =" + req.body);
	//console.log("Nombre =" + req.body.first_name);
	//console.log("Email =" + req.body.email);

	let newID = users_file.length + 1;
	let newUser = {
		"id":newID,
		"first_name": req.body.first_name,
    	"last_name": req.body.last_name,
    	"email": req.body.email,
    	"gender": req.body.gender,
    	"ip_address": req.body.ip_address
	}

	users_file.push(newUser);

	console.log("Usuario insertado " + newUser.first_name);

	res.send({"msg":"Usuario creado exitosamente"});
}

//app.put(global.URL_BASE + 'users',
function modificarUsuario(req,res){

	let idModificar = req.body.id;

	let newUser = {
		"id":idModificar,
		"first_name": req.body.first_name,
    	"last_name": req.body.last_name,
    	"email": req.body.email,
    	"gender": req.body.gender,
    	"password": req.body.password
	}

	users_file[idModificar-1] = newUser;

	console.log("Usuario modificado " + newUser.first_name);

	res.send({"msg":"Usuario modificado exitosamente"});
}

//DELETE falta tunearlo
//app.delete(global.URL_BASE + 'users/:idUsuario',
function eliminarUsuario(req,res){
	let idEliminar = req.params.idUsuario;

	if(users_file[idEliminar -1] != undefined){
		users_file.splice(idEliminar - 1,1);
		res.send({"msg":"Usuario eliminado exitosamente"});
	}else{
		res.send({"msg":"Usuario no encontrado"});
	}
}

//LOGIN
//app.post(global.URL_BASE + 'login',
/*function login(req,res){
	let usu = req.body.usuario;
	let email = req.body.correo;

	for(i=0;i<users_file.length;++i){
		if(users_file[i].email == email){
			if(users_file[i].first_name == usu){
				res.send({"msg":"Usuario logueado"});
			}else{
				res.send({"msg":"Usuario no existe con el correo"});
			}
		}else{
			res.send({"msg":"Correo no existe en BD"});
		}
	}
}*/

//app.post(global.URL_BASE + 'login',
function login(req, resp){
	console.log('Login');
	console.log(req.body.email);
	console.log(req.body.password);

	let idUsuario=0;

	users_file.forEach(user => {
		if(user.email==req.body.email && user.password==req.body.password){
			idUsuario=user.ID;
			user.logged=true;
			//break;
		}
	});

	writeUserDataToFile(users_file);

	if(idUsuario!=0){
		resp.send({"msg":"Login correcto", "id":idUsuario});
		}else{
		resp.send({"msg":"Login incorrecto"});
	}
}

//
/*app.delete(global.URL_BASE + 'logout/:idUsuario',function(req,res){
	let idEliminar = req.params.idUsuario;

	if(users_file[idEliminar -1] != undefined){
		users_file.splice(idEliminar - 1,1);
		res.send({"msg":"Usuario deslogueado"});
	}else{
		res.send({"msg":"Error no existe usuario"});
	}
});*/

//Logout
//app.post(global.URL_BASE + 'logout', 
function logout(req, resp){
	console.log('Logout');
	console.log(req.body.id);

	let idUsuario=0;
	
	users_file.forEach(user => {
		if(user.logged==true && user.ID==req.body.id){
			idUsuario=user.ID;
			delete user.logged;
			//break;
		}
	});

	writeUserDataToFile(users_file);
	
	if(idUsuario!=0){
		resp.send({"msg":"logout correcto", "id":idUsuario});
	}else{
		resp.send({"msg":"logout incorrecto"});
	}
}

//FUNCION PARA MODIFICAR JSON
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

 module.exports = {
 	obtenerUsuarioXid,
 	agregarUsuario,
	modificarUsuario,
	eliminarUsuario,
	login,
	logout
 }

/*app.listen(port, function () {
console.log('Example app listening on port 3000!');
});*/