require('dotenv').config();

const global = require('../global');
const apikeyMLab = 'apiKey=' + process.env.API_KEY_MLAB;
const baseMLabURL = process.env.BASE_MLAB_URL;

var requestJSON = require('request-json');

function login(req,res){
    let email = req.body.email;
    let pass = req.body.password;
    let queryLogin = 'users?q={"email":"' + email + '","password":"' + pass + '"}&';
    let filtro = 'l=1&';
    let cliente = requestJSON.createClient(baseMLabURL);

    cliente.get(queryLogin + filtro + apikeyMLab,
        function(erGet,respGet,bodyGet){
            if(!erGet){

                //console.log(bodyGet);

                if(bodyGet.length == 1){
                    let claveActualizar = '{"$set":{"logged":true}}';
                    let queryLoginExitoso = 'users?q={"id": ' + bodyGet[0].id + '}&' + apikeyMLab;

                    cliente.put(queryLoginExitoso,JSON.parse(claveActualizar),
                        function(erPut,respPut,bodyPut){
                            if(bodyPut.n==1){
                                res.status(200).send({'msg':'Login correcto','user':bodyGet[0].email,'name':bodyGet[0].first_name});
                            }else{
                                res.send({'msg':'Actualizacion no completada'});
                            }
                        });

                }else{
                    res.status(404).send({'msg':'No hay registro del usuario'});
                }
            }
        });
}

function logout(req,res){
    let email = req.body.email;
    let pass = req.body.password;
    let queryValidaUsuario = 'users?q={"email":"' + email + '","password":"' + pass + '"}&';
    let filtro = 'l=1&';
    let cliente = requestJSON.createClient(baseMLabURL);

    cliente.get(queryValidaUsuario + filtro + apikeyMLab,
        function(erGet,respGet,bodyGet){
            if(!erGet){

                //console.log(bodyGet);

                if(bodyGet.length == 1){
                    let claveActualizar = '{"$unset":{"logged":true}}';
                    let queryLogoutExitoso = 'users?q={"id": ' + bodyGet[0].id + '}&' + apikeyMLab;

                    //console.log(bodyGet[0]);

                    cliente.put(queryLogoutExitoso,JSON.parse(claveActualizar),
                        function(erPut,respPut,bodyPut){
                            if(bodyPut.n==1){
                                res.status(200).send({'msg':'Hasta pronto ' + bodyGet[0].first_name});
                            }else{
                                res.send({'msg':'Deslogueo no completado'});
                            }
                        });

                }else{
                    res.status(404).send({'msg':'No hay registro del usuario'});
                }
            }
        });
}

module.exports = {
    login,
    logout
  }