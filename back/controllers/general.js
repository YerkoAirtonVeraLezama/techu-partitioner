var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyparser = require('body-parser');

const global = require('../global');
var requestJSON = require('request-json');

require('dotenv').config();

const apikeyMLab = 'apiKey=' + process.env.API_KEY_MLAB;
const baseMLabURL = process.env.BASE_MLAB_URL;

app.use(bodyparser.json());

// GET users consumiendo API REST de mLab
function obtenerUsuarios(req,res){
   var httpClient = requestJSON.createClient(baseMLabURL);
   var queryString = 'f={"_id":0}&';
   
   //console.log(baseMLabURL + 'users?' + queryString + apikeyMLab);
   httpClient.get('users?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
}

// GET users consumiendo API REST de mLab
function obtenerUsuariosId(req,res){
  //console.log("GET /colapi/v3/users");
  var httpClient = requestJSON.createClient(baseMLabURL);
  //console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&q={"id": '+ req.body.idUser + '}&';
  //console.log(baseMLabURL + 'users?' + queryString + apikeyMLab);
  httpClient.get('users?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          //console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
}

function bbbb(req,res){
  var httpClient = requestJSON.createClient(baseMLabURL);
  //console.log(req.body);

  httpClient.get('users?' + apikeyMLab,
    function(error,req, res){
      
      newId = body.length +1;
      //console.log("newId" + newId);

      var newUser = {
        "id" : newId,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      }

      httpClient.post(baseMLabURL + "user?" + apikeyMLab, newUser,
        function(error,req, res){
          //console.log(body);
          res.status(201);
          res.send(body);
        });
    });

}

module.exports = {
  obtenerUsuarios,
  obtenerUsuariosId
}